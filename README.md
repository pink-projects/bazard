# Collection de Scripts Python

## Aperçu

Les scripts couvrent une large gamme de fonctionnalités, incluant des calculs mathématiques, des jeux interactifs, et des utilitaires pratiques pour la vie quotidienne. Les utilisateurs peuvent également explorer le cryptage de texte pour la sécurité des données et des fonctionnalités basées sur l'intelligence artificielle pour l'analyse avancée et la génération de contenu.


## Description des Scripts
- `calc.py` : Script pour des opérations de calcul de base.
- `convertisseur.py` : Ce code convertit une distance en mètres vers d'autres unités métriques spécifiées par l'utilisateur et affiche le résultat. Si l'unité n'est pas reconnue, il signale une erreur d'unité invalide.
- `deviner nombre.py` : Jeu où l'utilisateur doit deviner un nombre aléatoire généré par l'ordinateur.
- `FizzBuzz.py` : Solution au problème classique de programmation FizzBuzz.
- `imc.py` : Calcule l'Indice de Masse Corporelle (IMC) et évalue la catégorie de poids de l'utilisateur.
- `initiales.py` : Génère les initiales à partir d'un nom complet.
- `nbre premier.py` : Vérifie si un nombre donné est premier.
- `nombre mots.py` : Compte le nombre de mots dans un texte saisi par l'utilisateur.
- `parité.py` : Détermine si un nombre est pair ou impair.
- `pierre papier ciseau.py` : Implémente le jeu classique de pierre-papier-ciseaux contre l'ordinateur.
- `planificateur.py` : Le code définit une application console pour gérer une liste d'achats, permettant à l'utilisateur d'ajouter, de supprimer, d'afficher les produits, et de calculer le coût total. L'interaction avec l'utilisateur se fait par des commandes entrées au clavier, et le programme fonctionne en continu jusqu'à ce que l'utilisateur décide de quitter.
- `shape.py` : Définit une classe Rectangle qui permet de créer un objet rectangle avec une longueur et une largeur spécifiées. La classe offre trois méthodes pour calculer le périmètre, l'aire, et pour vérifier si le rectangle est en fait un carré (lorsque la longueur et la largeur sont égales)..
- `taches.py` : Gère une liste de tâches, permettant à l'utilisateur d'ajouter, supprimer et lister des tâches.
- `temperature.py` : Donne ma moyenne d'une liste de temperature.
- `turtles.py` : Exemple de création de graphiques et animations avec la bibliothèque Turtle.
- `cryptage.ipynb` : Ce code permet à l'utilisateur de saisir un texte, qui est ensuite inversé et sauvegardé dans un nouveau fichier.
- `nuage_mots.ipynb` : Ce code analyse un texte pour compter la fréquence des mots, excluant certains mots courants, et enregistre les résultats dans un fichier JSON pour une visualisation potentielle sous forme de nuage de mots.

## Prérequis
- Python 3.x
- Aucune dépendance externe n'est nécessaire pour la majorité des scripts, sauf mention contraire dans le code source.

## Installation
Aucune installation spécifique n'est requise autre que d'avoir Python installé sur votre machine.

## Utilisation
Chaque script peut être exécuté individuellement depuis la ligne de commande. Pour lancer un script, naviguez dans le dossier contenant le script et exécutez :

```bash
python nom_du_script.py
```
