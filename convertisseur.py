

conv={"km":10**3,"hm":10**2,"da":10,"m":1,"dm":10**(-1),"cm":10**(-2),"mm":10**(-3)}

def convo(distance,unite):

    if (unite in conv):

        conver= distance/conv[unite]
        print(f"Conversion: {distance} m = {conver} {unite}")
    else:
        print("unité invalide")

distance= int(input("entrer distance (m) :\n"))
unite= input("entrer unité : \n").lower()
convo(distance,unite)